from flask_login import current_user
from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed, FileField
from PIL import Image
from wtforms import PasswordField, StringField, SubmitField
from wtforms.validators import EqualTo, InputRequired, Length, ValidationError

from app.models import User


class EditAvatarForm(FlaskForm):
    avatar = FileField('Update Avatar',
                       validators=[InputRequired(), FileAllowed(['jpg', 'png', 'jpeg'])])
    submit = SubmitField('Update')


class EditUsernameForm(FlaskForm):
    username = StringField('Username',
                           validators=[
                               InputRequired(), Length(min=3, max=20)
                           ])
    submit = SubmitField('Update')

    def validate_username(self, username):
        if username.data != current_user.username:
            users = User.query.filter_by(username=username.data).all()
            if len(users):
                raise ValidationError('Username has already been taken.')


class EditPasswordForm(FlaskForm):
    password = PasswordField('Password',
                             validators=[
                                 InputRequired(), Length(min=8, max=50)
                             ])
    new_password = PasswordField('New Password',
                                 validators=[
                                     InputRequired(),
                                     Length(min=8, max=50),
                                     EqualTo('confirm_new_password',
                                             'New Passwords must match.')
                                 ])
    confirm_new_password = PasswordField('Confirm New Password',
                                         validators=[
                                             InputRequired(), Length(min=8, max=50)
                                         ])
    submit = SubmitField('Update')


class DeleteProfileForm(FlaskForm):
    password = PasswordField('Password',
                             validators=[
                                 InputRequired(),
                                 Length(min=8, max=50),
                                 EqualTo('confirm_password',
                                         'Passwords must match.')
                             ])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[
                                         InputRequired(), Length(min=8, max=50)
                                     ])
    submit = SubmitField('Delete')


class UploadForm(FlaskForm):
    image = FileField('Upload Image',
                      validators=[InputRequired(), FileAllowed(['jpg', 'png', 'jpeg'])])
    submit = SubmitField('Upload')