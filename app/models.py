from flask_login import UserMixin

from app import db, login_manager


class Upload(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    image = db.Column(db.String(255), nullable=False, unique=True)
    creation_date = db.Column(db.DateTime(), nullable=False)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    avatar = db.Column(db.String(255), nullable=False, default='default.jpg')
    username = db.Column(db.String(20), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    creation_date = db.Column(db.DateTime(), nullable=False)
    is_active = db.Column(db.Boolean(), nullable=False, default=True)
    uploads = db.relationship('Upload', backref='uploaded_by', lazy=True)


@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))
