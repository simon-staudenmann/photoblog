from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField, SubmitField
from wtforms.validators import EqualTo, InputRequired, Length, ValidationError

from app.models import User


class LoginForm(FlaskForm):
    username = StringField('Username',
                           validators=[
                               InputRequired(), Length(min=3, max=20)
                           ])
    password = PasswordField('Password',
                             validators=[
                                 InputRequired(), Length(min=8, max=50)
                             ])
    submit = SubmitField('Login')


class RegisterForm(FlaskForm):
    username = StringField('Username',
                           validators=[
                               InputRequired(), Length(min=3, max=20)
                           ])
    password = PasswordField('Password',
                             validators=[
                                 InputRequired(),
                                 Length(min=8, max=50),
                                 EqualTo('confirm_password', 'Passwords must match.')
                             ])
    confirm_password = PasswordField('Confirm Password',
                                     validators=[
                                         InputRequired(), Length(min=8, max=50)
                                     ])
    submit = SubmitField('Register')

    def validate_username(self, username):
        users = User.query.filter_by(username=username.data).all()
        if len(users):
            raise ValidationError('Username has already been taken.')
