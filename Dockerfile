FROM python

COPY requirements.txt /app/
WORKDIR /app
RUN pip install -r requirements.txt
COPY . /app

CMD ["python", "photoblog.py"]