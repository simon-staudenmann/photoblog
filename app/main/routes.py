from datetime import datetime
from flask import flash, redirect, render_template, request, Response, url_for
from flask_login import current_user, login_required, logout_user
from PIL import Image
from uuid import uuid4

from app import bcrypt, db
from app.main import bp
from app.main.forms import EditAvatarForm, EditUsernameForm, EditPasswordForm, DeleteProfileForm, UploadForm
from app.models import User, Upload


@bp.route('/')
def index():
    return redirect(url_for('main.home'))


@bp.route('/home')
def home():
    page = request.args.get('page', 1, type=int)
    uploads = Upload.query.order_by(
        Upload.creation_date.desc()).paginate(page=page, per_page=10)
    return render_template('main/home.html', uploads=uploads)


@bp.route('/profile')
@login_required
def profile():
    return redirect(url_for('main.users', username=current_user.username))

@bp.route('/users/<username>')
def users(username):
    page = request.args.get('page', 1, type=int)
    user = User.query.filter_by(username=username,
                                is_active=True).first_or_404()
    uploads = Upload.query.filter_by(user_id=user.id).order_by(
        Upload.creation_date.desc()).paginate(page=page, per_page=10)
    return render_template('main/user.html', user=user, uploads=uploads)


@bp.route('/profile/avatar/edit', methods=['GET', 'POST'])
@login_required
def edit_avatar():
    form = EditAvatarForm()
    if form.validate_on_submit():
        avatar = Image.open(form.avatar.data)
        if avatar.width == avatar.height:
            uuid = uuid4()
            complete_path = f'app/static/avatars/{uuid}.jpg'
            path = f'avatars/{uuid}.jpg'
            avatar.save(complete_path)
            current_user.avatar = path
            db.session.commit()
            flash('Avatar updated', 'success')
            return Response(status=200)
        else:
            flash('Invalid avatar dimensions', 'danger')
            return Response(status=400)
    return render_template('main/edit_avatar.html', form=form)


@bp.route('/profile/username/edit', methods=['GET', 'POST'])
@login_required
def edit_username():
    form = EditUsernameForm()
    if form.validate_on_submit():
        if current_user.username != form.username.data:
            current_user.username = form.username.data
            db.session.commit()
            flash('Username updated', 'success')
        return redirect(url_for('main.users', username=current_user.username))
    form.username.data = current_user.username
    return render_template('main/edit_username.html', form=form)


@bp.route('/profile/password/edit', methods=['GET', 'POST'])
@login_required
def edit_password():
    form = EditPasswordForm()
    if form.validate_on_submit():
        if bcrypt.check_password_hash(current_user.password, form.password.data):
            current_user.password = bcrypt.generate_password_hash(
                form.new_password.data)
            db.session.commit()
            flash('Password updated', 'success')
            return redirect(url_for('main.users', username=current_user.username))
        else:
            flash('Invalid Password', 'danger')
    return render_template('main/edit_password.html', form=form)


@bp.route('/profile/delete', methods=['GET', 'POST'])
@login_required
def delete_profile():
    form = DeleteProfileForm()
    if form.validate_on_submit():
        if bcrypt.check_password_hash(current_user.password, form.password.data):
            current_user.is_active = False
            db.session.commit()
            logout_user()
            flash('Profile deleted', 'success')
            return redirect(url_for('main.home'))
        else:
            flash('Invalid password', 'danger')
    return render_template('main/delete_profile.html', form=form)


@bp.route('/upload', methods=['GET', 'POST'])
@login_required
def upload():
    form = UploadForm()
    if form.validate_on_submit():
        image = Image.open(form.image.data)
        if image.width == image.height:
            uuid = uuid4()
            complete_path = f'app/static/images/{uuid}.jpg'
            path = f'images/{uuid}.jpg'
            image.save(complete_path)
            upload = Upload(
                image=path, creation_date=datetime.now(), user_id=current_user.id)
            db.session.add(upload)
            db.session.commit()
            flash('Image uploaded', 'success')
            return Response(status=200)
        else:
            flash('Invalid image dimensions', 'danger')
            return Response(status=400)
    return render_template('main/upload.html', form=form)
